package com.example.josh.khoaluan.content;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.accountservices.UserSingleton;
import com.example.josh.khoaluan.accountsettings.AccountSetting_;
import com.example.josh.khoaluan.classmain.CreateClassActivity_;
import com.example.josh.khoaluan.classmain.JoinInClass_;
import com.example.josh.khoaluan.login.profilesettings.ProfileSettingActivity_;
import com.example.josh.khoaluan.passwordconfig.ChangePasswordActivity_;
import com.example.josh.khoaluan.utils.AppUtil;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import de.hdodenhof.circleimageview.CircleImageView;

@EFragment(R.layout.fragment_menu)
public class MenuFragment extends Fragment {
    public MenuFragment() {
    }

    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment_();
        return fragment;
    }

    @ViewById(R.id.fragment_menu_nestedscrollview)
    NestedScrollView scrollView;

    @ViewById(R.id.fragment_menu_fadingimage)
    ImageView fadingImage;

    @ViewById(R.id.fragment_menu_avatar)
    ImageView circleAvatar;

    @ViewById(R.id.fragment_menu_addclass_holder)
    RelativeLayout addClassHolder;

    @ViewById(R.id.fragment_menu_btn_profilesettings)
    TextView btnProfileSetting;
    @AfterViews
    public void initViews(){
        AppUtil.addFadingFeatureScrollView(getActivity(), scrollView, fadingImage);
        loadUserData();
    }


    public void loadUserData(){
        //learner
        if(UserSingleton.USER_TYPE == 1 && UserSingleton.getUserLearner()!=null)
        {
            Learner learner = UserSingleton.getUserLearner();
            circleAvatar.setImageResource(R.drawable.ic_userlearner);
            addClassHolder.setVisibility(View.GONE);
            btnProfileSetting.setVisibility(View.GONE);
        }
        //teacher

        if(UserSingleton.USER_TYPE == 2 && UserSingleton.getUserTeacher()!=null){
            Teacher teacher = UserSingleton.getUserTeacher();
            Picasso.get().load(teacher.getTeacherPic()).into(circleAvatar);
            addClassHolder.setVisibility(View.VISIBLE);
            btnProfileSetting.setVisibility(View.VISIBLE);
        }
    }

    @Click(R.id.fragment_menu_btn_addclass)
    public void goCreateClass(){
        CreateClassActivity_.intent(this).start();
    }

    @Click(R.id.fragment_menu_btn_logout)
    public void goLogout(){
        getActivity().finish();
    }

    @Click(R.id.fragment_menu_btn_pw)
    public void changePassword(){
        ChangePasswordActivity_.intent(this).start();
    }

    @Click(R.id.fragment_menu_btn_profilesettings)
    public void settingProfile(){
        ProfileSettingActivity_.intent(this).start();
    }

    @Click(R.id.fragment_menu_btn_accountsettings)
    public void settingAccount() {
        AccountSetting_.intent(this).start();
    }
}
