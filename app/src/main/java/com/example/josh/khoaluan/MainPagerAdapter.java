package com.example.josh.khoaluan;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MainPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> arrayFragment;
    private FragmentManager fm;
    private Activity activity;

    public MainPagerAdapter(Activity activity, FragmentManager fm, ArrayList<Fragment> arrayFragment) {
        super(fm);
        this.arrayFragment = arrayFragment;
        this.fm = fm;
        this.activity = activity;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        return arrayFragment.get(position);
    }

    @Override
    public int getCount() {
        return arrayFragment.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }
}
