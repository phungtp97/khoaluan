package com.example.josh.khoaluan.passwordconfig;

import android.content.Context;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.example.josh.khoaluan.accountservices.UserSingleton;

public class ChangePasswordPresenter implements ChangePasswordListener{
    ChangePasswordView changePasswordView;
    Context context;

    public ChangePasswordPresenter(ChangePasswordView changePasswordView, Context context) {
        this.changePasswordView = changePasswordView;
        this.context = context;
    }

    public void updateData(String id, String password){
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(this, context);
        changePasswordRequest.changePassword(password);
    }

    @Override
    public void onChangePwLearnerSuccess(Learner learner) {
        UserSingleton.setLearnerUser(learner);
        changePasswordView.onMessageSuccess("Update info success");
    }

    @Override
    public void onChangePwTeacherSuccess(Teacher teacher) {
        UserSingleton.setTeacherUser(teacher);
        changePasswordView.onMessageSuccess("Update info success");
    }

    @Override
    public void onChangePwFailed(String string) {
        changePasswordView.onMessageError(string);
    }
}
