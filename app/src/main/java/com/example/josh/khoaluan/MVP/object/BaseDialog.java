package com.example.josh.khoaluan.MVP.object;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.utils.AppUtil;

public class BaseDialog extends Dialog {
    TextView tvLabel;
    EditText etContent;
    ImageView btnNegative;
    ImageView btnPositive;
    int mTag = 0;
    BaseDialogListener listener;
    public BaseDialog(Context context) {
        super(context);
        setContentView(R.layout.dialog_edit_action);
        tvLabel = findViewById(R.id.dialog_edit_action_label);
        etContent = findViewById(R.id.dialog_et);
        btnNegative = findViewById(R.id.basedialog_iv_negativebtn);
        btnPositive = findViewById(R.id.basedialog_iv_positivebtn);
        AppUtil.performDepthChangedOnPress(context, btnPositive);
    }

    public BaseDialog setLabel(String string){
        tvLabel.setText(string);
        return this;
    }

    public BaseDialog setTag(int tag){
        mTag = tag;
        return this;
    }

    public int getTag(){
        return mTag;
    }

    public BaseDialog setContent(String string, Boolean editable){
        etContent.setText(string);
        /*if(editable==true){
            etContent.setBackgroundResource(R.drawable.bg_textview_stroke_rectangle_with_selector);
        }
        else{
            etContent.setBackgroundResource(R.color.transparent);
        }*/
        etContent.setEnabled(editable);
        return this;
    }

    public BaseDialog setHintContent(String string){
        etContent.setHint(string);
        return this;
    }

    public BaseDialog setOnlyOneButton(Boolean bool){
        if(bool == true) {
            btnNegative.setVisibility(View.GONE);
        }
        else{
            btnNegative.setVisibility(View.VISIBLE);
        }
        return this;
    }

    public BaseDialog setClickListener(BaseDialogListener baseDialogListener){
        this.listener = baseDialogListener;
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.doOnClickButtonRight();
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.doOnClickButtonLeft(etContent.getText().toString());
            }
        });
        return this;
    }

}
