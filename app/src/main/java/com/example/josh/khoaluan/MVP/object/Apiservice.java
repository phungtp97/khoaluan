package com.example.josh.khoaluan.MVP.object;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.teacher.Teacher;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Apiservice {

    @GET("api/Classes")
    Call<List<MyClass>> getAllClasses();

    @GET("api/CATEGORIes")
    Call<List<CategoryApi>> getAllCatgories();

    @GET("api/LEVELs")
    Call<List<Level>> getAllLevels();

    @GET("api/LEARNERs/{id}")
    Call<Learner> getLearnerById(@Path("id") String id);

    @GET("api/TEACHERs/{id}")
    Call<Teacher> getTeacherById(@Path("id") String id);

    @GET("api/CLASSMONITORs/{id}")
    Call<ClassMonitor> getClassMonitorById(@Path("id") String id);

    //create new learner account
    @POST("api/LEARNERs")
    @FormUrlEncoded
    Call<Learner> postLearner(@Field("learnerID") String learnerID,
                              @Field("learnerEmail") String learnerEmail,
                              @Field("learnerPassword") String learnerPassword);


    //create new teacher account
    @POST("api/TEACHERs")
    @FormUrlEncoded
    Call<Teacher> postTeacher(@Field("teacherID") String teacherID,
                              @Field("teacherEmail") String teacherEmail,
                              @Field("teacherPassword") String teacherPassword);

    @POST("api/CLASSes")
    Call<MyClass> postClass(@Body MyClass myClass
                            /*@Field("classTitle") String classTitle,
                            @Field("classDetail") String classDetail,
                            @Field("createDay") String createDay,
                            @Field("closeapplyDay") String closeapplyDay,
                            @Field("startDay") String startDay,
                            @Field("endDay") String endDay,
                            @Field("available") Boolean available,
                            @Field("classPrice") Float classPrice,
                            @Field("studentNumber") int studentNumber,
                            @Field("minimumCapacity") int minimumCapacity,
                            @Field("maximumCapacity") int maximumCapacity,
                            @Field("category") String category,
                            @Field("teacherID") String teacherID,
                            @Field("serviceFee") Float serviceFee,
                            @Field("filterWard") String filterWard,
                            @Field("filterCity") String filterCity,
                            @Field("levelDebscibe") String levelDebscibe*/
            );

    @POST("api/CLASSMONITORs")
    Call<ClassMonitor> postMonitor(@Body ClassMonitor classMonitor);

    //update learner basic profile
    @PUT("api/LEARNERs/{id}")
    @FormUrlEncoded
    Call<Learner> updateLearnerBasicProfile(@Path("id") String ID,
                                            @Field("learnerEmail") String learnerEmail,
                                            @Field("learnerPhone") String learnerPhone,
                                            @Field("learnerPassword") String learnerPassword);


    @PUT("api/LEARNERs/{id}")
    @FormUrlEncoded
    Call<Learner> changePasswordLearner(@Path("id") String ID,
                                        @Field("learnerID") String learnerID,
                                        @Field("learnerEmail") String learnerEmail,
                                        @Field("learnerPassword") String learnerPassword);

    @PUT("api/TEACHERs/{id}")
    @FormUrlEncoded
    Call<Teacher> changePasswordTeacher(@Path("id") String ID,
                                        @Field("teacherID") String teacherID,
                                        @Field("teacherEmail") String teacherEmail,
                                        @Field("teacherPassword") String teacherPassword);

    //update teacher basic profile
    @PUT("api/TEACHERs/{id}")
    @FormUrlEncoded
    Call<Teacher> updateTeacherBasicProfile(@Path("id") String ID,
                                            @Body Teacher teacher
                                            /*@Field("learnerID") String learnerID,
                                            @Field("learnerEmail") String learnerEmail,
                                            @Field("learnerPhone") String learnerPhone,
                                            @Field("learnerName") String learnerName,
                                            @Field("teacherBirth") String learnerBirth,
                                            @Field("teacherGender") Boolean learnerGender,
                                            @Field("teacherPrice") Float learnerPrice,
                                            @Field("teacherRate") int teacherRate,
                                            @Field("teacherDetail") String teacherDetail,
                                            @Field("teacherActive") Boolean teacherActive,
                                            @Field("teacherVerify") Boolean teacherVerify,
                                            @Field("learnerPic") String learnerPic,
                                            @Field("learnerPassword") String learnerPassword*/);

    @PUT("api/LEVELs/{id}")
    @FormUrlEncoded
    Call<Level> updateLevel(@Path("id") String ID,
                              @Field("levelDebscibe") String levelDebscibe);
}
