package com.example.josh.khoaluan.content;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.josh.khoaluan.MVP.object.MyClass;
import com.example.josh.khoaluan.MainRecyclerViewAdapter;
import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.classmain.JoinInClass_;
import com.example.josh.khoaluan.utils.AppUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EFragment(R.layout.fragment_main)
public class MainFragment extends Fragment implements AllClassView, MainRecyclerViewAdapter.OnItemClickListener {

    @ViewById(R.id.fragment_main_recycler_view)
    RecyclerView mainRecyclerView;

    MainRecyclerViewAdapter adapter;

    AllClassPresenter presenter;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment_();
        return fragment;
    }

    @AfterViews
    public void loadData() {
        AppUtil.init(getActivity());
        presenter = new AllClassPresenter(this, getActivity());
        presenter.loadData();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDisplayAllClass(List<MyClass> classList) {
        adapter = new MainRecyclerViewAdapter(getActivity(), classList, this);
        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getActivity());
        mainRecyclerView.setLayoutManager(eLayoutManager);
        mainRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onDisplayError(String message) {
        Toast.makeText(getActivity(), "Errors : "+message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onReadMore(int position) {

    }

    @Override
    public void onReview(MyClass myClass) {
        AppUtil.setViewClass(myClass);
        JoinInClass_.intent(getActivity()).start();
    }

    @Override
    public void onSaved(int position) {

    }
}
