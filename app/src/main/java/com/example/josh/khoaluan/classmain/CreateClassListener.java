package com.example.josh.khoaluan.classmain;

import com.example.josh.khoaluan.MVP.object.MyClass;

public interface CreateClassListener {
    void onCreateClassSucceed(MyClass myClass);
    void onCreateClassFailed(String message);
}
