package com.example.josh.khoaluan.login;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.teacher.Teacher;

public interface LoginView {
    void onDisplayLearner(Learner learner);
    void onDisplayTeacher(Teacher teacher);
    void onDisplayError(String error);
}
