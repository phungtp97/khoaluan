package com.example.josh.khoaluan.login.signup;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.object.Apiservice;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupLearnerRequest {
    SignupLearnerListener signupLearnerListener;
    Context context;
    ProgressDialog pDialog;

    public SignupLearnerRequest(SignupLearnerListener signupLearnerListener, Context context) {
        this.signupLearnerListener = signupLearnerListener;
        this.context = context;
    }
    public void logIn(String id, String email, String password){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Data.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        Apiservice api = RetrofitClient.getApiService();

        Call<Learner> call = api.postLearner(id, email, password);
        call.enqueue(new Callback<Learner>() {
            @Override
            public void onResponse(Call<Learner> call, Response<Learner> response) {
                pDialog.dismiss();
                if(response.isSuccessful()) {
                    signupLearnerListener.onPostLearnerSucceed(response.body());
                }
                if(!response.isSuccessful()){
                    signupLearnerListener.onPostLearnerFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<Learner> call, Throwable t) {
                pDialog.dismiss();
                signupLearnerListener.onPostLearnerFailed(t.getMessage());
            }
        });
    }

}
