package com.example.josh.khoaluan.content;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.josh.khoaluan.MVP.RecyclerViewAdapter;
import com.example.josh.khoaluan.MVP.object.MyClass;
import com.example.josh.khoaluan.MVP.view.ViewMVP;
import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.classmain.CreateClassActivity_;
import com.example.josh.khoaluan.classmain.JoinInClass_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_content)
public class ContentActivity extends AppCompatActivity {
    MyPagerAdapter adapterViewPager;

    @ViewById(R.id.activity_content_viewpg)
    ViewPager vpPager;

    @AfterViews
    public void afterView(){
        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        DepthTransformation depthTransformation = new DepthTransformation();
        vpPager.setPageTransformer(true, depthTransformation);
        vpPager.setAdapter(adapterViewPager);
        vpPager.setCurrentItem(1);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return 2;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return MenuFragment_.newInstance();
                case 1:
                    return MainFragment_.newInstance();
                default:
                    return MainFragment_.newInstance();
            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }

    public void goJoinClass(View view){
        JoinInClass_.intent(this).start();
    }

}
