package com.example.josh.khoaluan.classmain;

import com.example.josh.khoaluan.MVP.object.MyClass;

public interface CreateClassView {
    void onDisplayClass(MyClass myClass);
    void onDisplayError(String message);
}
