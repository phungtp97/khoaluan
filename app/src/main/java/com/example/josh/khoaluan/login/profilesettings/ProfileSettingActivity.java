package com.example.josh.khoaluan.login.profilesettings;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.base.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.w3c.dom.Text;

@EActivity(R.layout.activity_profile_setting)
public class ProfileSettingActivity extends BaseActivity implements View.OnClickListener {

    @ViewById(R.id.profilesetting_info)
    EditText btnInfo;


    @ViewById(R.id.profilesetting_birth)
    TextView btnBirth;

    @ViewById(R.id.profilesetting_price)
    EditText btnPrice;

    Dialog dialogEditText;

    Dialog dialogGender;

    Dialog dialogDayPicker;

    Dialog dialogPrice;

    TextView tvDialogText;

    EditText etDialogText;

    TextView tvDialogPrice;

    EditText etDialogPrice;

    RadioGroup rbtnGender;

    RadioButton rbtnMale;

    RadioButton rbtnFemale;

    @AfterViews
    public void afterViews() {
        initDialog();
    }

    public void initDialog() {

    }

    @Override
    public void onClick(View v) {

    }
}
