package com.example.josh.khoaluan.login;

import android.content.Context;

import com.example.josh.khoaluan.MVP.teacher.Teacher;

public class LoginTeacherPresenter implements LoginTeacherListener {
    LoginView loginView;
    Context context;

    public LoginTeacherPresenter(LoginView loginView, Context context) {
        this.loginView = loginView;
        this.context = context;
    }

    public void loadData(String id, String password){
        LoginTeacherRequest loginTeacherRequest = new LoginTeacherRequest(this, context);
        loginTeacherRequest.logIn(id, password);
    }


    @Override
    public void onLoadTeacherSucceed(Teacher teacher) {
        loginView.onDisplayTeacher(teacher);
    }

    @Override
    public void onLoadTeacherFailed(String message) {
        loginView.onDisplayError(message);
    }
}
