package com.example.josh.khoaluan.passwordconfig;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.accountservices.UserSingleton;
import com.example.josh.khoaluan.base.BaseActivity;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_change_password)
public class ChangePasswordActivity extends BaseActivity implements ChangePasswordView {
    @ViewById(R.id.changepw_et_pw)
    EditText etPassword;
    @ViewById(R.id.changepw_et_username)
    EditText etUsername;

    @Click(R.id.changepw_btn_changepw)
    public void changePassword(){
        ChangePasswordPresenter presenter = new ChangePasswordPresenter(this, this);
        presenter.updateData(UserSingleton.getUserLearner().getLearnerID(), etPassword.getText().toString());
    }

    @Override
    public void onMessageSuccess(String string) {
        Toast.makeText(this, string, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMessageError(String string) {
        Toast.makeText(this, string, Toast.LENGTH_LONG).show();
    }
}
