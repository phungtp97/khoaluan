package com.example.josh.khoaluan.MyAnimation;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.example.josh.khoaluan.R;


public class AnimationManager implements AnimationInterface{
    Context mContext;
    View mView;
    Animation mAnimation;
    Boolean mSlide = false;

    public AnimationManager(Context context, View view){
        this.mContext = context;
        this.mView = view;
    }
    @Override
    public void setSlideUp(Boolean slideup) {
        this.mSlide = slideup;
        if(mSlide = true){
            mAnimation = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
            mView.startAnimation(mAnimation);
            mView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setSlideDown(Boolean slidedown) {
        this.mSlide = slidedown;
        if(mSlide = true){
            mAnimation = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);
            mView.startAnimation(mAnimation);
            mView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSlideLeft(Boolean slideleft) {
        this.mSlide = slideleft;
        if(mSlide = true){
            mAnimation = AnimationUtils.loadAnimation(mContext, R.anim.slide_left);
            mView.startAnimation(mAnimation);
            mView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSlideRight(Boolean slideright) {
        this.mSlide = slideright;
        if(mSlide = true){
            mAnimation = AnimationUtils.loadAnimation(mContext, R.anim.slide_right);
            mView.startAnimation(mAnimation);
            mView.setVisibility(View.GONE);
        }
    }

    @Override
    public View getLayout() {
        return mView;
    }


}
