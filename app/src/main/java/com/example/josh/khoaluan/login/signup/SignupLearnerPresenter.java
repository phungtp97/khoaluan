package com.example.josh.khoaluan.login.signup;

import android.content.Context;

import com.example.josh.khoaluan.MVP.learner.Learner;

public class SignupLearnerPresenter implements SignupLearnerListener {
    SignupView signupView;
    Context context;

    public SignupLearnerPresenter(SignupView signupView, Context context) {
        this.signupView = signupView;
        this.context = context;
    }

    public void loadData(String id, String email, String password){
        SignupLearnerRequest signupLearnerRequest = new SignupLearnerRequest(this, context);
        signupLearnerRequest.logIn(id, email, password);
    }


    @Override
    public void onPostLearnerSucceed(Learner learner) {
        signupView.onLearnerPostSucceed(learner);
    }

    @Override
    public void onPostLearnerFailed(String message) {
        signupView.onDisplayError(message);
    }
}
