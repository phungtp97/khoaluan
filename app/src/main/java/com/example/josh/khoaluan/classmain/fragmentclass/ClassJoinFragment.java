package com.example.josh.khoaluan.classmain.fragmentclass;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.object.ClassMonitor;
import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.accountservices.UserSingleton;
import com.example.josh.khoaluan.utils.AppUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_class_join)
public class ClassJoinFragment extends Fragment {

    @ViewById(R.id.fragmentjoin_postmonitor)
    Button btnPost;

    public ClassJoinFragment() {

    }
    public static ClassJoinFragment newInstance() {
        ClassJoinFragment fragment = new ClassJoinFragment_();
        return fragment;
    }

    @AfterViews
    public void init(){

    }

    @Click(R.id.fragmentjoin_postmonitor)
    public void postMonitor(){
        Apiservice api = RetrofitClient.getApiService();
        ClassMonitor classMonitor = new ClassMonitor();
        classMonitor.setMonitorID(UUID.randomUUID().toString());
        classMonitor.setLearnerID(UserSingleton.getUserLearner().getLearnerID());
        classMonitor.setClassID(AppUtil.getViewClass().getClassID());
        Call<ClassMonitor> call = api.postMonitor(classMonitor);
        call.enqueue(new Callback<ClassMonitor>() {
            @Override
            public void onResponse(Call<ClassMonitor> call, Response<ClassMonitor> response) {
                if(response.isSuccessful()){
                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getActivity().finish();
                        }
                    }, 1000);
                }
            }

            @Override
            public void onFailure(Call<ClassMonitor> call, Throwable t) {

            }
        });
    }
}
