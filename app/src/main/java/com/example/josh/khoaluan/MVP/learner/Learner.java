package com.example.josh.khoaluan.MVP.learner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

public class Learner {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("learnerID")
    @Expose
    private String learnerID;
    @SerializedName("learnerEmail")
    @Expose
    private String learnerEmail;
    @SerializedName("learnerPhone")
    @Expose
    private String learnerPhone="";
    @SerializedName("personalBalance")
    @Expose
    private Float personalBalance=0.0f;
    @SerializedName("learnerVerify")
    @Expose
    private Boolean learnerVerify=false;
    @SerializedName("learnerActive")
    @Expose
    private Boolean learnerActive=false;
    @SerializedName("learnerPassword")
    @Expose
    private String learnerPassword = "123456";

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getLearnerID() {
        return learnerID;
    }

    public void setLearnerID(String learnerID) {
        this.learnerID = learnerID;
    }

    public String getLearnerEmail() {
        return learnerEmail;
    }

    public void setLearnerEmail(String learnerEmail) {
        this.learnerEmail = learnerEmail;
    }

    public String getLearnerPhone() {
        if(learnerPhone==null) learnerPhone = "";
            return learnerPhone;
    }

    public void setLearnerPhone(String learnerPhone) {
        this.learnerPhone = learnerPhone;
    }

    public Float getPersonalBalance() {
        if(personalBalance==null) personalBalance = 0.0f;
        return personalBalance;
    }

    public void setPersonalBalance(Float personalBalance) {
        this.personalBalance = personalBalance;
    }

    public Boolean getLearnerVerify() {
        return learnerVerify;
    }

    public void setLearnerVerify(Boolean learnerVerify) {
        this.learnerVerify = learnerVerify;
    }

    public Boolean getLearnerActive() {
        return learnerActive;
    }

    public void setLearnerActive(Boolean learnerActive) {
        this.learnerActive = learnerActive;
    }

    public String getLearnerPassword() {
        return learnerPassword;
    }

    public void setLearnerPassword(String learnerPassword) {
        this.learnerPassword = learnerPassword;
    }

    /*public String exportJson(){
        return "{\n" +
                "  \"learnerID\": \""+learnerID+"\",\n" +
                "  \"learnerEmail\": \""+learnerEmail+"\",\n" +
                "  \"learnerPhone\": \""+learnerPhone+"\",\n" +
                "  \"learnerName\": \""+learnerName+"\",\n" +
                "  \"learnerBirth\": \""+learnerBirth+"\",\n" +
                "  \"personalBalance\": 1.1,\n" +
                "  \"learnerVerify\": true,\n" +
                "  \"learnerActive\": true,\n" +
                "  \"learnerPassword\": \""+learnerPassword+"\"\n" +
                "}";

    }*/
}
