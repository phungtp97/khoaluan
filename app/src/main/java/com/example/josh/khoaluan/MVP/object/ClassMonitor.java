package com.example.josh.khoaluan.MVP.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClassMonitor {

    @SerializedName("monitorID")
    @Expose
    private String monitorID;
    @SerializedName("learnerID")
    @Expose
    private String learnerID;
    @SerializedName("classID")
    @Expose
    private String classID;

    public String getMonitorID() {
        return monitorID;
    }

    public void setMonitorID(String monitorID) {
        this.monitorID = monitorID;
    }

    public String getLearnerID() {
        return learnerID;
    }

    public void setLearnerID(String learnerID) {
        this.learnerID = learnerID;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

}

