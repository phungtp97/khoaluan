package com.example.josh.khoaluan.login.signup;

import android.content.Context;

import com.example.josh.khoaluan.MVP.teacher.Teacher;

public class SignupTeacherPresenter implements SignupTeacherListener {
    SignupView signupView;
    Context context;

    public SignupTeacherPresenter(SignupView signupView, Context context) {
        this.signupView = signupView;
        this.context = context;
    }

    public void loadData(String id, String email,String password){
        SignupTeacherRequest signupTeacherRequest = new SignupTeacherRequest(this, context);
        signupTeacherRequest.logIn(id, email, password);
    }


    @Override
    public void onPostTeacherSucceed(Teacher teacher) {
        signupView.onTeacherPostSucceed(teacher);
    }

    @Override
    public void onPostTeacherFailed(String message) {
        signupView.onDisplayError(message);
    }
}
