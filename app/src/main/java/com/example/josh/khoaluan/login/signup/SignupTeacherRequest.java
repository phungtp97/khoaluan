package com.example.josh.khoaluan.login.signup;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.teacher.Teacher;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupTeacherRequest {

    SignupTeacherListener signupTeacherListener;
    Context context;
    ProgressDialog pDialog;

    public SignupTeacherRequest(SignupTeacherListener signupTeacherListener, Context context) {
        this.signupTeacherListener = signupTeacherListener;
        this.context = context;
    }
    public void logIn(String id, String email, String password){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Getting response of creating your Teacher account....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        Apiservice api = RetrofitClient.getApiService();

        Call<Teacher> call = api.postTeacher(id, email, password);

        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                pDialog.dismiss();
                if(response.isSuccessful()){
                    signupTeacherListener.onPostTeacherSucceed(response.body());
                }
                if(!response.isSuccessful()){
                    signupTeacherListener.onPostTeacherFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                pDialog.dismiss();
                signupTeacherListener.onPostTeacherFailed(t.getMessage());
            }
        });
    }

}
