package com.example.josh.khoaluan.EhancedEditText;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ToggleButton;

import com.example.josh.khoaluan.R;


public class EhancedEditTextBuilder {
    boolean isHighlightBorder = false;
    boolean isFlexblePasswordInputType = false;
    boolean isLimitedInputType = false;
    boolean isEmailInputType = false;
    int inputLimit = 0;
    EhancedEditTextLayout ehancedEditTextLayout;
    String hintText;
    Button btnForCheck;
    Boolean isWarning;
    Boolean isChecked;
    int drawableLeftResid;
    int hintColorResid;
    Boolean numberInput;
    onCheckLimitedInputListener mListener;


    public EhancedEditTextBuilder(EhancedEditTextLayout ehancedEditTextLayout) {
        this.ehancedEditTextLayout = ehancedEditTextLayout;
    }

    private boolean isHighlightBorder() {
        return isHighlightBorder;
    }

    private boolean isFlexblePasswordInputType() {
        return isFlexblePasswordInputType;
    }

    private boolean isLimitedInputType() {
        return isLimitedInputType;
    }

    private boolean isEmailInputType() {
        return isEmailInputType;
    }

    private boolean getWarning() {
        return isWarning;
    }

    private int getDrawableLeftResid() {
        return drawableLeftResid;
    }

    public int getHintColorResid() {
        return hintColorResid;
    }

    public boolean getNumberInput() {
        return numberInput;
    }

    public EhancedEditTextBuilder highlightBorder(Boolean isHighlightBorder) {
        this.isHighlightBorder = isHighlightBorder;
        return this;
    }

    public EhancedEditTextBuilder flexblePasswordInputType(boolean isFlexblePasswordInputType) {
        this.isFlexblePasswordInputType = isFlexblePasswordInputType;
        return this;
    }

    public EhancedEditTextBuilder limitedInputType(boolean isLimitedInputType, int inputLimit, onCheckLimitedInputListener listener) {
        this.isLimitedInputType = isLimitedInputType;
        this.inputLimit = inputLimit;
        this.mListener = listener;
        return this;
    }

    public EhancedEditTextBuilder EmailInputType(boolean isEmailInputType, @Nullable Button checkBtn) {
        this.isEmailInputType = isEmailInputType;
        this.btnForCheck = checkBtn;
        return this;
    }

    public EhancedEditTextBuilder hintText(String hintText) {
        this.hintText = hintText;
        return this;
    }

    public EhancedEditTextBuilder Warning(Boolean isWarn) {
        this.isWarning = isWarn;
        return this;
    }

    public EhancedEditTextBuilder iconLeft(int Resid) {
        this.drawableLeftResid = Resid;
        return this;
    }

    public EhancedEditTextBuilder hintColor(int Resid) {
        this.hintColorResid = Resid;
        return this;
    }

    public EhancedEditTextBuilder inputNumberOnly(boolean numberInput){
        this.numberInput = numberInput;
        return this;
    }

    public EditText getEditTextComponent() {
        EditText etEhancedEditText = ehancedEditTextLayout.findViewById(R.id.et_ehanced_edittext_layout);
        return etEhancedEditText;
    }

    public Button getButtonComponent() {
        Button etEhancedButton = ehancedEditTextLayout.findViewById(R.id.btn_ehanced_edittext_layout);
        return etEhancedButton;
    }

    private void getEhancedView() {

        int resLayoutID = R.layout.ehanced_edittext;
        ehancedEditTextLayout.addView(LayoutInflater
                .from(ehancedEditTextLayout.getContext())
                .inflate(resLayoutID, null));
        int resBorderID = R.drawable.bg_textview_normalstatestroke_rectangle;
        final EditText etEhancedEditText = ehancedEditTextLayout.findViewById(R.id.et_ehanced_edittext_layout);
        etEhancedEditText.setHint(hintText);
        final ToggleButton btnEhancedEditText = ehancedEditTextLayout.findViewById(R.id.btn_ehanced_edittext_layout);
        if(numberInput!=null&&numberInput==true){
            etEhancedEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        if (getDrawableLeftResid() != 0) {
            Drawable img = etEhancedEditText.getContext().getResources().getDrawable(drawableLeftResid);
            if (getHintColorResid() != 0) {
                DrawableCompat.wrap(img);
                DrawableCompat.setTint(img, etEhancedEditText.getResources().getColor(getHintColorResid()));
            }
            img.setBounds(0, 0, 60, 60);
            etEhancedEditText.setCompoundDrawables(img, null, null, null);
            etEhancedEditText.setCompoundDrawablePadding(24);
        }
        if (this.isEmailInputType() == true) {
            etEhancedEditText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            if (btnForCheck != null) {
                etEhancedEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (etEhancedEditText.getText().toString().matches("^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$")) {
                            btnForCheck.setEnabled(true);
                        } else {
                            btnForCheck.setEnabled(false);
                        }
                    }
                });
            }
            if (isWarning == true) {

                etEhancedEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus == true) {
                            isChecked = true;
                            if (hasFocus == false) {
                                if (etEhancedEditText.getText().toString().matches("^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$")) {
                                    etEhancedEditText.setBackgroundResource(R.drawable.bg_textview_stroke_rectangle_with_selector);
                                } else {
                                    etEhancedEditText.setBackgroundResource(R.drawable.bg_textview_warnedstroke_rectangle);
                                }
                            }
                        }
                    }
                });
            }
        }
        if (this.isFlexblePasswordInputType() == true) {
            btnEhancedEditText.setVisibility(View.VISIBLE);
            btnEhancedEditText.setEnabled(true);
            btnEhancedEditText.setBackgroundResource(R.drawable.ic_eye);
            etEhancedEditText.setTransformationMethod(new PasswordTransformationMethod());
            btnEhancedEditText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked == true) {
                        etEhancedEditText.setTransformationMethod(null);
                        etEhancedEditText.setSelection(etEhancedEditText.length());
                    } else {
                        etEhancedEditText.setTransformationMethod(new PasswordTransformationMethod());
                        etEhancedEditText.setSelection(etEhancedEditText.length());
                    }
                    Log.d("Checked", String.valueOf(isChecked));
                }
            });
            if (isWarning == true) {

                etEhancedEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus == true) {
                            isChecked = true;
                            if (hasFocus == false) {
                                if (etEhancedEditText.getText().toString().matches("((?=.*\\d).{6,18})")) {
                                    etEhancedEditText.setBackgroundResource(R.drawable.bg_textview_stroke_rectangle_with_selector);
                                } else {
                                    etEhancedEditText.setBackgroundResource(R.drawable.bg_textview_warnedstroke_rectangle);
                                }
                            }
                        }
                    }
                });
            }
        }
        if (this.isLimitedInputType() == true) {

            btnEhancedEditText.setVisibility(View.VISIBLE);
            etEhancedEditText.setMaxEms(inputLimit);
            etEhancedEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (etEhancedEditText.length() <= 5) {
                        btnEhancedEditText.setBackgroundResource(R.color.transparent);
                        mListener.onInvalid();
                    }
                    if (etEhancedEditText.length() == 6) {
                        btnEhancedEditText.setBackgroundResource(R.drawable.ic_checkmark);
                        mListener.onPassed(etEhancedEditText.getText().toString());
                    }
                    if (etEhancedEditText.length() > 6) {
                        etEhancedEditText.setText(etEhancedEditText.getText().toString().substring(0, etEhancedEditText.length()));
                    }
                }
            });
        }

        if (this.isHighlightBorder() == true) {
            resBorderID = R.drawable.bg_textview_stroke_rectangle_with_selector;
        }

        etEhancedEditText.setBackgroundResource(resBorderID);
    }

    public EhancedEditTextLayout build() {
        getEhancedView();
        return ehancedEditTextLayout;
    }

}
