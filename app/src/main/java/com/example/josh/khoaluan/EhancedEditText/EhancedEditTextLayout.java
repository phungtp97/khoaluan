package com.example.josh.khoaluan.EhancedEditText;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class EhancedEditTextLayout extends LinearLayout {
    public EhancedEditTextLayout(Context context) {
        super(context);
    }

    public EhancedEditTextLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EhancedEditTextLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}

