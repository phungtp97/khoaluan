package com.example.josh.khoaluan.login.signup;

import com.example.josh.khoaluan.MVP.learner.Learner;

public interface SignupLearnerListener {
    void onPostLearnerSucceed(Learner learner);
    void onPostLearnerFailed(String message);
}
