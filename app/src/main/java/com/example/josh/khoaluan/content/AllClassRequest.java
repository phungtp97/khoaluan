package com.example.josh.khoaluan.content;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.object.MyClass;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllClassRequest {
    AllClassListener allClassListener;
    Context context;
    ProgressDialog pDialog;
    List<MyClass> myClassList;

    public AllClassRequest(AllClassListener allClassListener, Context context) {
        this.allClassListener = allClassListener;
        this.context = context;
    }
    public void loadAllClass(){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Data.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        Apiservice api = RetrofitClient.getApiService();
        Call<List<MyClass>> callClassList = api.getAllClasses();
        callClassList.enqueue(new Callback<List<MyClass>>() {
            @Override
            public void onResponse(Call<List<MyClass>> call, Response<List<MyClass>> response) {
                pDialog.dismiss();
                if(response.isSuccessful()){
                    allClassListener.onLoadAllClassSucceed(response.body());
                }
                else{
                    allClassListener.onLoadAllClassFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<MyClass>> call, Throwable t) {
                pDialog.dismiss();
                allClassListener.onLoadAllClassFailed(t.getMessage());
            }
        });
    }
}
