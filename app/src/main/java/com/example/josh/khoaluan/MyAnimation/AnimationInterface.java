package com.example.josh.khoaluan.MyAnimation;

import android.view.View;
import android.widget.LinearLayout;

public interface AnimationInterface {
    void setSlideUp(Boolean slideup);
    void setSlideDown(Boolean slidedown);
    void setSlideLeft(Boolean slideleft);
    void setSlideRight(Boolean slideright);
    View getLayout();
}
