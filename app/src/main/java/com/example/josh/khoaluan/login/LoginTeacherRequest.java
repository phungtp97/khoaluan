package com.example.josh.khoaluan.login;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.example.josh.khoaluan.accountservices.UserSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginTeacherRequest {

    LoginTeacherListener loginTeacherListener;
    Context context;
    ProgressDialog pDialog;

    public LoginTeacherRequest(LoginTeacherListener loginTeacherListener, Context context) {
        this.loginTeacherListener = loginTeacherListener;
        this.context = context;
    }
    public void logIn(String id, final String password){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Data.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        Apiservice api = RetrofitClient.getApiService();

        Call<Teacher> call = api.getTeacherById(id);
        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if(response.body()!=null){
                    if(response.body().getTeacherPassword().replaceAll(" ","").equals(password)){
                        pDialog.dismiss();
                        loginTeacherListener.onLoadTeacherSucceed(response.body());
                        UserSingleton.USER_TYPE = 2;
                    }
                    else{
                        pDialog.dismiss();
                        loginTeacherListener.onLoadTeacherFailed("Wrong password");
                    }
                }
                else{
                    pDialog.dismiss();
                    loginTeacherListener.onLoadTeacherFailed("Wrong password");
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                pDialog.dismiss();
                loginTeacherListener.onLoadTeacherFailed(t.getMessage());
            }
        });
    }

}
