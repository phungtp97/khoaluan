package com.example.josh.khoaluan.accountservices;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.teacher.Teacher;

public class UserSingleton {
    public static int USER_TYPE = 0; // 1 for learner, 2 for teacher
    private static Learner learnerUser;
    private static Teacher teacherUser;

    public static Learner getUserLearner() {
        if (learnerUser == null) {
            learnerUser = new Learner();
        }
        return learnerUser;
    }

    public static Teacher getUserTeacher() {
        if (teacherUser == null) {
            teacherUser = new Teacher();
        }
        return teacherUser;
    }

    public static void setLearnerUser(Learner learner){
        learnerUser = learner;
    }

    public static void setTeacherUser(Teacher teacher){
        teacherUser = teacher;
    }

}
