package com.example.josh.khoaluan.MVP.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryApi {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("category1")
    @Expose
    private String category1;

    public String getCategory1() {
        return category1;
    }

    @Override
    public String toString() {
        return category1;
    }
}
