package com.example.josh.khoaluan.login;

import com.example.josh.khoaluan.MVP.learner.Learner;

public interface LoginLearnerListener {
    void onLoadLearnerSucceed(Learner learner);
    void onLoadLearnerFailed(String message);
}
