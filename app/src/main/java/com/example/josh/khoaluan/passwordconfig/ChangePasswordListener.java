package com.example.josh.khoaluan.passwordconfig;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.teacher.Teacher;

public interface ChangePasswordListener {
    void onChangePwLearnerSuccess(Learner learner);
    void onChangePwTeacherSuccess(Teacher teacher);
    void onChangePwFailed(String response);

}
