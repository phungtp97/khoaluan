package com.example.josh.khoaluan.classmain;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.object.ClassMonitor;
import com.example.josh.khoaluan.MVP.object.MyClass;
import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.example.josh.khoaluan.MainPagerAdapter;
import com.example.josh.khoaluan.MainRecyclerViewAdapter;
import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.accountservices.UserSingleton;
import com.example.josh.khoaluan.classmain.fragmentclass.ClassContentFragment;
import com.example.josh.khoaluan.classmain.fragmentclass.ClassContentFragment_;
import com.example.josh.khoaluan.classmain.fragmentclass.ClassJoinFragment;
import com.example.josh.khoaluan.classmain.fragmentclass.ClassJoinFragment_;
import com.example.josh.khoaluan.classmain.fragmentclass.ClassReviewFragment;
import com.example.josh.khoaluan.classmain.fragmentclass.ClassReviewFragment_;
import com.example.josh.khoaluan.content.DepthTransformation;
import com.example.josh.khoaluan.utils.AppUtil;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.w3c.dom.Text;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EActivity(R.layout.activity_join_in_class)
public class JoinInClass extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    @ViewById(R.id.joinin_class_pager)
    ViewPager mainViewPager;

    public static MyClass reviewedClass;

    @ViewById(R.id.joinclass_ll_rate)
    LinearLayout llRate;

    @ViewById(R.id.joinclass_tv_name)
    TextView tvName;

    @ViewById(R.id.joinclass_tv_price)
    TextView tvPrice;

    @ViewById(R.id.joinclass_tv_daycreate)
    TextView tvDayCreate;

    @ViewById(R.id.joinclass_tv_title)
    TextView tvTitle;

    @ViewById(R.id.joinclass_iv_avatar)
    ImageView ivAvatar;

    @ViewById(R.id.joinclass_day_closeapply)
    TextView tvExpiredDay;

    @ViewById(R.id.joinclass_studytime)
    TextView tvStudyTime;

    @ViewById(R.id.joinclass_tb_content)
    ToggleButton tbContent;

    @ViewById(R.id.joinclass_tb_review)
    ToggleButton tbReview;

    @ViewById(R.id.joinclass_tv_join)
    TextView btnJoin;

    @ViewById(R.id.joinclass_tb_price)
    ToggleButton tbPrice;

    @ViewById(R.id.joinclass_tb_save)
    ToggleButton tbSave;

    @AfterViews
    public void afterViews() {
        ArrayList<Fragment> fragmentArrayList = new ArrayList<>();
        fragmentArrayList.add(ClassContentFragment_.newInstance());
        fragmentArrayList.add(ClassReviewFragment_.newInstance());
        if(UserSingleton.USER_TYPE==1) {
            Apiservice api = RetrofitClient.getApiService();
            Call<ClassMonitor> call = api.getClassMonitorById(AppUtil.getViewClass().getClassID());
            call.enqueue(new Callback<ClassMonitor>() {
                @Override
                public void onResponse(Call<ClassMonitor> call, Response<ClassMonitor> response) {
                }

                @Override
                public void onFailure(Call<ClassMonitor> call, Throwable t) {
                    fragmentArrayList.add(ClassJoinFragment_.newInstance());
                }
            });
        }
        DepthTransformation depthTransformation = new DepthTransformation();
        mainViewPager.setAdapter(new MainPagerAdapter(this, getSupportFragmentManager(), fragmentArrayList));
        mainViewPager.setOffscreenPageLimit(fragmentArrayList.size());
        mainViewPager.setCurrentItem(0);
        mainViewPager.setPageTransformer(true, depthTransformation);
        if (AppUtil.getViewClass().getClassID() != null) {
            setClass(AppUtil.getViewClass());
            Log.d("message", AppUtil.getViewClass().getClassID());
        }
    }

    public void setClass(MyClass myclass) {
        reviewedClass = myclass;
        String name = reviewedClass.getTeacherName();
        String dayCreated = AppUtil.toMMddYYYY(reviewedClass.getCreateDay());
        String title = reviewedClass.getClassTitle();
        String dayExpired = AppUtil.toMMddYYYY(reviewedClass.getCloseapplyDay());
        String dayStarted = AppUtil.toMMddYYYY(reviewedClass.getStartDay());
        String dayEnded = AppUtil.toMMddYYYY(reviewedClass.getEndDay());
        Float price = reviewedClass.getClassPrice();
        int rate = reviewedClass.getTeacherRate();
        String teacherPic = reviewedClass.getTeacherPic();

        tvName.setText(name);
        tvPrice.setText(String.valueOf(price));
        tvTitle.setText(title);
        tvDayCreate.setText("Created in: " + dayCreated);
        tvExpiredDay.setText("Expired in: " + dayExpired);
        tvStudyTime.setText("From " + dayStarted + " to " + dayEnded);
        try {
            Picasso.get().load(teacherPic).into(ivAvatar);
        } catch (IllegalArgumentException e) {
            Picasso.get().load(teacherPic).into(ivAvatar);
        }
        //process rate and display
        for (int i = 1; i <= rate / 2; i++) {
            View star = new View(this);
            star.setLayoutParams(
                    new LinearLayout.LayoutParams(
                            AppUtil.dpToPx(
                                    this, 24), AppUtil.dpToPx(
                            this, 24)));
            llRate.addView(star);
            star.setBackgroundResource(R.drawable.ic_star);
            star.setBackgroundTintList(this.getResources().getColorStateList(R.color.yellow));
        }
        if (rate % 2 == 1) {
            View star = new View(this);
            star.setLayoutParams(
                    new LinearLayout.LayoutParams(
                            AppUtil.dpToPx(
                                    this, 24), AppUtil.dpToPx(
                            this, 24)));
            llRate.addView(star);
            star.setBackgroundResource(R.drawable.ic_star_half);
            star.setBackgroundTintList(this.getResources().getColorStateList(R.color.yellow));
        }
        tbContent.setOnCheckedChangeListener(this);
        tbReview.setOnCheckedChangeListener(this);
        tbPrice.setOnCheckedChangeListener(this);
        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                tbContent.setChecked(false);
                tbReview.setChecked(false);
                tbPrice.setChecked(false);
                tbContent.setEnabled(true);
                tbReview.setEnabled(true);
                tbPrice.setEnabled(true);
                if(i==0){
                    tbContent.setChecked(true);
                    tbContent.setEnabled(false);
                }
                if(i==1){
                    tbReview.setChecked(true);
                    tbReview.setEnabled(false);
                }
                if(i==2){
                    tbPrice.setChecked(true);
                    tbPrice.setEnabled(false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView==tbContent){
            if(isChecked==true){
                tbContent.setBackgroundResource(R.drawable.ic_content);
                mainViewPager.setCurrentItem(0);
            }
            else{
                tbContent.setBackgroundResource(R.drawable.ic_contentdisable);
            }
        }
        if(buttonView==tbReview){
            if(isChecked==true){
                mainViewPager.setCurrentItem(1);
                tbReview.setBackgroundResource(R.drawable.ic_review);
            }
            else{
                tbReview.setBackgroundResource(R.drawable.ic_reviewdisable);
            }
        }
        if(buttonView==tbPrice){
            if(isChecked==true){
                mainViewPager.setCurrentItem(2);
                tbPrice.setBackgroundResource(R.drawable.ic_price);
            }
            else{
                tbPrice.setBackgroundResource(R.drawable.ic_pricedisable);
            }
        }

    }
}
