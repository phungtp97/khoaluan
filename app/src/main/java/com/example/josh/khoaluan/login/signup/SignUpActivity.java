package com.example.josh.khoaluan.login.signup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.example.josh.khoaluan.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_sign_up)
public class SignUpActivity extends AppCompatActivity implements SignupView {

    @ViewById(R.id.signup_et_username)
    EditText etUsername;

    @ViewById(R.id.signup_et_email)
    EditText etEmail;

    @ViewById(R.id.signup_pw)
    EditText etPassword;

    @ViewById(R.id.signup_confirm_pw)
    EditText etPasswordConfirm;

    @ViewById(R.id.signup_btn_signup)
    TextView btnSignUp;

    @ViewById(R.id.signup_rbtn_group)
    RadioGroup rbtnGroup;

    SignupLearnerPresenter signupLearnerPresenter;
    SignupTeacherPresenter signupTeacherPresenter;

    @AfterViews
    public void afterView() {
        signupLearnerPresenter = new SignupLearnerPresenter(this, this);

        signupTeacherPresenter = new SignupTeacherPresenter(this, this);
    }

    @Click(R.id.signup_btn_signup)
    public void onClick() {
        if (rbtnGroup.getCheckedRadioButtonId() == R.id.signup_rbtn_learner) {
            signupLearnerPresenter.loadData(etUsername.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString());
        }
        if (rbtnGroup.getCheckedRadioButtonId() == R.id.signup_rbtn_teacher) {
            signupTeacherPresenter.loadData(etUsername.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString());
        }
    }

    @Override
    public void onLearnerPostSucceed(Learner learner) {
        Toast.makeText(this, "Post Learner success", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTeacherPostSucceed(Teacher teacher) {
        Toast.makeText(this, "Post Teacher success", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDisplayError(String error) {
        Toast.makeText(this, "Sign up failed: " + error, Toast.LENGTH_LONG).show();
    }
}
