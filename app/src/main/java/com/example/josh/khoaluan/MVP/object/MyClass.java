package com.example.josh.khoaluan.MVP.object;

import android.os.Parcelable;

import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyClass implements Serializable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("classID")
    @Expose
    private String classID;
    @SerializedName("classTitle")
    @Expose
    private String classTitle;
    @SerializedName("classDetail")
    @Expose
    private String classDetail;
    @SerializedName("createDay")
    @Expose
    private String createDay;
    @SerializedName("closeapplyDay")
    @Expose
    private String closeapplyDay;
    @SerializedName("startDay")
    @Expose
    private String startDay;
    @SerializedName("endDay")
    @Expose
    private String endDay;
    @SerializedName("available")
    @Expose
    private Boolean available;
    @SerializedName("classPrice")
    @Expose
    private Float classPrice;
    @SerializedName("studentNumber")
    @Expose
    private int studentNumber;
    @SerializedName("minimumCapacity")
    @Expose
    private int minimumCapacity;
    @SerializedName("maximumCapacity")
    @Expose
    private int maximumCapacity;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("teacherID")
    @Expose
    private String teacherID;

    @SerializedName("teacherName")
    @Expose
    private String teacherName;
    @SerializedName("teacherRate")
    @Expose
    private int teacherRate;
    @SerializedName("teacherPic")
    @Expose
    private String teacherPic;

    @SerializedName("serviceFee")
    @Expose
    private Float serviceFee;
    @SerializedName("filterWard")
    @Expose
    private String filterWard;
    @SerializedName("filterCity")
    @Expose
    private String filterCity;
    @SerializedName("levelDebscibe")
    @Expose
    private String levelDebscibe;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getClassTitle() {
        return classTitle;
    }

    public void setClassTitle(String classTitle) {
        this.classTitle = classTitle;
    }

    public String getClassDetail() {
        return classDetail;
    }

    public void setClassDetail(String classDetail) {
        this.classDetail = classDetail;
    }

    public String getCreateDay() {
        return createDay;
    }

    public void setCreateDay(String createDay) {
        this.createDay = createDay;
    }

    public String getCloseapplyDay() {
        return closeapplyDay;
    }

    public void setCloseapplyDay(String closeapplyDay) {
        this.closeapplyDay = closeapplyDay;
    }

    public String getStartDay() {
        return startDay;
    }

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    public String getEndDay() {
        return endDay;
    }

    public void setEndDay(String endDay) {
        this.endDay = endDay;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Float getClassPrice() {
        return classPrice;
    }

    public void setClassPrice(Float classPrice) {
        this.classPrice = classPrice;
    }

    public int getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(int studentNumber) {
        this.studentNumber = studentNumber;
    }

    public int getMinimumCapacity() {
        return minimumCapacity;
    }

    public void setMinimumCapacity(int minimumCapacity) {
        this.minimumCapacity = minimumCapacity;
    }

    public int getMaximumCapacity() {
        return maximumCapacity;
    }

    public void setMaximumCapacity(int maximumCapacity) {
        this.maximumCapacity = maximumCapacity;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public Float getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(Float serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getFilterWard() {
        return filterWard;
    }

    public void setFilterWard(String filterWard) {
        this.filterWard = filterWard;
    }

    public String getFilterCity() {
        return filterCity;
    }

    public void setFilterCity(String filterCity) {
        this.filterCity = filterCity;
    }

    public String getLevelDebscibe() {
        return levelDebscibe;
    }

    public void setLevelDebscibe(String levelDebscibe) {
        this.levelDebscibe = levelDebscibe;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherPic() {
        return teacherPic;
    }

    public void setTeacherPic(String teacherPic) {
        this.teacherPic = teacherPic;
    }

    public int getTeacherRate() {
        return teacherRate;
    }

    public void setTeacherRate(int teacherRate) {
        this.teacherRate = teacherRate;
    }
}