package com.example.josh.khoaluan.passwordconfig;

public interface ChangePasswordView {
    void onMessageSuccess(String string);
    void onMessageError(String string);
}
