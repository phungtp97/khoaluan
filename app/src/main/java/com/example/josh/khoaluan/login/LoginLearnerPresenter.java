package com.example.josh.khoaluan.login;

import android.content.Context;

import com.example.josh.khoaluan.MVP.learner.Learner;

public class LoginLearnerPresenter implements LoginLearnerListener {
    LoginView loginView;
    Context context;

    public LoginLearnerPresenter(LoginView loginView, Context context) {
        this.loginView = loginView;
        this.context = context;
    }

    public void loadData(String id, String password){
        LoginLearnerRequest loginLearnerRequest = new LoginLearnerRequest(this, context);
        loginLearnerRequest.logIn(id, password);
    }

    @Override
    public void onLoadLearnerSucceed(Learner learner) {
        loginView.onDisplayLearner(learner);
    }

    @Override
    public void onLoadLearnerFailed(String message) { loginView.onDisplayError(message);

    }
}
