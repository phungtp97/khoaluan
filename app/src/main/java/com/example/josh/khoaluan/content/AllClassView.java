package com.example.josh.khoaluan.content;

import com.example.josh.khoaluan.MVP.object.MyClass;

import java.util.List;

public interface AllClassView {
    void onDisplayAllClass(List<MyClass> classList);
    void onDisplayError(String message);
}
