package com.example.josh.khoaluan.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.base.BaseActivity;

public class ResetPasswordActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
    }
}
