package com.example.josh.khoaluan.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.example.josh.khoaluan.MVP.object.BaseDialog;
import com.example.josh.khoaluan.MVP.object.BaseDialogListener;
import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.utils.AppUtil;
import com.example.josh.khoaluan.utils.KeyboardVisibility;

public class BaseActivity extends Activity {
    private Dialog progressDialogLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showProgressLoading() {

        if (progressDialogLoading != null)
            progressDialogLoading.dismiss();
        View view = getLayoutInflater().inflate(R.layout.layout_progress_loading, null);
        RotateLoading rotateloading = (RotateLoading) view.findViewById(R.id.layout_loading_rotate);
        rotateloading.start();
        progressDialogLoading = new Dialog(this);
        progressDialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialogLoading.setContentView(view);
        progressDialogLoading.setCancelable(false);
        progressDialogLoading.setCanceledOnTouchOutside(false);

        Window window = progressDialogLoading.getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.bg_layout_loading);
        }

        progressDialogLoading.show();
    }

    public void hideProgressLoading() {
        if (progressDialogLoading != null && progressDialogLoading.isShowing())
            progressDialogLoading.dismiss();
    }

    public void showBaseDialog(Context context,String title, String content,Boolean contentEditable, @Nullable BaseDialogListener baseDialogListener){
        BaseDialog baseDialog = new BaseDialog(context)
                .setLabel(title)
                .setContent(content, contentEditable)
                .setHintContent("Edit something");
        if(baseDialogListener!=null){
            baseDialog.setClickListener(baseDialogListener);
        }
    }
}
