package com.example.josh.khoaluan.MVP.object;

public interface BaseDialogListener {
    void doOnClickButtonLeft(String content);
    void doOnClickButtonRight();
}
