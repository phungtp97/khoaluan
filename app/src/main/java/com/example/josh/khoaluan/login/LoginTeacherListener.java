package com.example.josh.khoaluan.login;

import com.example.josh.khoaluan.MVP.teacher.Teacher;

public interface LoginTeacherListener {
    void onLoadTeacherSucceed(Teacher teacher);
    void onLoadTeacherFailed(String message);
}
