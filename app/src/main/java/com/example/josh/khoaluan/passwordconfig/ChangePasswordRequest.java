package com.example.josh.khoaluan.passwordconfig;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.example.josh.khoaluan.accountservices.UserSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordRequest {
    ChangePasswordListener changePasswordListener;
    Context context;
    ProgressDialog pDialog;

    public ChangePasswordRequest(ChangePasswordListener changePasswordListener, Context context) {
        this.changePasswordListener = changePasswordListener;
        this.context = context;
    }
    public void changePassword(String password){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Data.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        Apiservice api = RetrofitClient.getApiService();

        //learner
        if(UserSingleton.USER_TYPE==1){
            Call<Learner> call = api.changePasswordLearner(UserSingleton.getUserLearner().getLearnerID() ,UserSingleton.getUserLearner().getLearnerID(), UserSingleton.getUserLearner().getLearnerEmail(), password);
            call.enqueue(new Callback<Learner>() {
                @Override
                public void onResponse(Call<Learner> call, Response<Learner> response) {
                    pDialog.dismiss();
                    if(response.body()!=null){
                        UserSingleton.setLearnerUser(response.body());
                    }
                    else{
                        changePasswordListener.onChangePwFailed(response.message());
                    }
                }

                @Override
                public void onFailure(Call<Learner> call, Throwable t) {
                    pDialog.dismiss();
                    changePasswordListener.onChangePwFailed(t.getMessage());
                }
            });
        }
        else{
            Call<Teacher> call = api.changePasswordTeacher(UserSingleton.getUserTeacher().getTeacherID(),
                    UserSingleton.getUserTeacher().getTeacherID(),
                    UserSingleton.getUserTeacher().getTeacherEmail(),
                    password);
            call.enqueue(new Callback<Teacher>() {
                @Override
                public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                    pDialog.dismiss();
                    if(response.body()!=null){
                        changePasswordListener.onChangePwTeacherSuccess(response.body());
                    }
                    else{
                        changePasswordListener.onChangePwFailed(response.message());
                    }
                }

                @Override
                public void onFailure(Call<Teacher> call, Throwable t) {
                    pDialog.dismiss();
                    changePasswordListener.onChangePwFailed(t.getMessage());
                }
            });
        }
    }
}
