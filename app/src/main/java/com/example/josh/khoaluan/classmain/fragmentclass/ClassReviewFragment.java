package com.example.josh.khoaluan.classmain.fragmentclass;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.content.MenuFragment;
import com.example.josh.khoaluan.content.MenuFragment_;

import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_class_review)
public class ClassReviewFragment extends Fragment {
    public ClassReviewFragment() {

    }
    public static ClassReviewFragment newInstance(){
        ClassReviewFragment fragment = new ClassReviewFragment_();
        return fragment;
    }

}
