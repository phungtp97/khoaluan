package com.example.josh.khoaluan.login.signup;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.teacher.Teacher;

public interface SignupView {
    void onLearnerPostSucceed(Learner learner);
    void onTeacherPostSucceed(Teacher teacher);
    void onDisplayError(String error);
}
