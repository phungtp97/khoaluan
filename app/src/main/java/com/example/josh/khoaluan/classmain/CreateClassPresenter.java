package com.example.josh.khoaluan.classmain;

import android.content.Context;

import com.example.josh.khoaluan.MVP.object.MyClass;

public class CreateClassPresenter implements CreateClassListener{
    Context context;
    CreateClassView classView;

    public CreateClassPresenter(Context context, CreateClassView classView) {
        this.context = context;
        this.classView = classView;
    }

    public void loadData(MyClass myClass){
        CreateClassRequest createClassRequest = new CreateClassRequest(this, context);
        createClassRequest.loadData(myClass);
    }

    @Override
    public void onCreateClassSucceed(MyClass myClass) {
        classView.onDisplayClass(myClass);
    }

    @Override
    public void onCreateClassFailed(String message) {
        classView.onDisplayError(message);
    }
}
