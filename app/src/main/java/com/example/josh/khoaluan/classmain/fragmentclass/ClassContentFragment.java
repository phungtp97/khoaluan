package com.example.josh.khoaluan.classmain.fragmentclass;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.utils.AppUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_class_content)
public class ClassContentFragment extends Fragment {

    @ViewById(R.id.fragmentcontent_content)
    TextView tvContent;

    @ViewById(R.id.fragmentcontent_title)
    TextView tvTitle;
    public ClassContentFragment() {

    }

    public static ClassContentFragment newInstance() {
        ClassContentFragment fragment = new ClassContentFragment_();
        return fragment;
    }
    @AfterViews
    public void init(){
        if(AppUtil.getViewClass().getClassID()!=null){
            tvContent.setText(AppUtil.getViewClass().getClassDetail());
            tvTitle.setText(AppUtil.getViewClass().getClassTitle());
        }
    }
}
