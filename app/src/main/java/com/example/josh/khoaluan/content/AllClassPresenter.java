package com.example.josh.khoaluan.content;

import android.content.Context;

import com.example.josh.khoaluan.MVP.object.MyClass;

import java.util.List;

public class AllClassPresenter implements AllClassListener{
    AllClassView allClassView;
    Context context;

    public AllClassPresenter(AllClassView allClassView, Context context) {
        this.allClassView = allClassView;
        this.context = context;
    }

    public void loadData(){
        AllClassRequest allClassRequest = new AllClassRequest(this, context);
        allClassRequest.loadAllClass();
    }


    @Override
    public void onLoadAllClassSucceed(List<MyClass> classList) {
         allClassView.onDisplayAllClass(classList);
    }

    @Override
    public void onLoadAllClassFailed(String message) {
        allClassView.onDisplayError(message);
    }
}
