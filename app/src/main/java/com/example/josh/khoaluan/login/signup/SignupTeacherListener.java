package com.example.josh.khoaluan.login.signup;

import com.example.josh.khoaluan.MVP.teacher.Teacher;

public interface SignupTeacherListener {
    void onPostTeacherSucceed(Teacher teacher);
    void onPostTeacherFailed(String message);
}
