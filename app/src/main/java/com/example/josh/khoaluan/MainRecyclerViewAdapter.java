package com.example.josh.khoaluan;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.object.MyClass;
import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.example.josh.khoaluan.utils.AppUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.MyViewHolder> {

    public interface OnItemClickListener {
        void onReadMore(int position);

        void onReview(MyClass myClass);

        void onSaved(int position);

    }

    List<MyClass> mClassList;

    Activity mActivity;

    OnItemClickListener mListener;

    int i;

    private List<MyClass> classList;

    private Teacher teacher;

    private List<Teacher> teacherList;

    public MainRecyclerViewAdapter(Activity activity, List<MyClass> classList, OnItemClickListener listener) {
        this.mClassList = classList;
        this.mActivity = activity;
        this.mListener = listener;
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_class_post, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        MyClass child = mClassList.get(position);
        holder.tvTitle.setText(child.getClassTitle());
        holder.tvDayCreate.setText("Created in: "+(AppUtil.toMMddYYYY(child.getCreateDay())));
        holder.tvContent.setText(child.getClassDetail());
        holder.tvName.setText(child.getTeacherName());
        try {
            Picasso.get().load(child.getTeacherPic()).into(holder.btnShowProfile);
        } catch (IllegalArgumentException e) {
            Picasso.get().load(R.drawable.ic_userlearner).into(holder.btnShowProfile);
        }

        holder.btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSaved(position);
            }
        });
        holder.btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onReview(mClassList.get(position));
            }
        });
        holder.btnReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.btnReadMore.isChecked() == true)
                    AppUtil.HeightChangedAnimation(mActivity, holder.tvContent, 700, 146);
                else
                    AppUtil.HeightChangedAnimation(mActivity, holder.tvContent, 700, -146);
                mListener.onReadMore(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mClassList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox btnLike;
        public CircleImageView btnShowProfile;
        public TextView btnReview;
        public ToggleButton btnReadMore;
        public TextView tvName, tvTitle, tvContent, tvDayCreate;


        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.classpost_teachername);
            tvContent = view.findViewById(R.id.classpost_content);
            tvDayCreate = view.findViewById(R.id.classpost_daycreate);
            tvTitle = view.findViewById(R.id.classpost_title);
            btnShowProfile = view.findViewById(R.id.class_post_btn_show_profile);
            btnLike = view.findViewById(R.id.class_post_like);
            btnReadMore = view.findViewById(R.id.class_post_readmore);
            btnReview = view.findViewById(R.id.class_post_review);
        }
    }
}

