package com.example.josh.khoaluan.MVP;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.MVP.object.MyClass;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CustomViewHolder> {
    private List<MyClass> classes;
    private int maxsize = 4;
    Context mContext;
    public RecyclerViewAdapter(List<MyClass> classes, Context context) {
        this.classes = classes;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_view_data_dataholder, parent, false);

        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        MyClass myClass = classes.get(position);
        //holder.tvDayLeft.setText(myClass.getCreateDay());
        holder.tvCategory.setText("Category: "+myClass.getCategory());
        holder.tvPrice.setText("$" + myClass.getClassPrice());
        holder.tvTitle.setText(myClass.getClassTitle());
        holder.tvContent.setText(myClass.getClassDetail());
        holder.tvTeacher.setText(myClass.getTeacherID());
        holder.tvCapacity.setText(myClass.getMaximumCapacity()+"/"+myClass.getMaximumCapacity());
        Picasso.get().load("https://s3.amazonaws.com/systemgravatars/avatar_6225.jpg").into(holder.ivAvatar);
        Picasso.get().load("https://s3.amazonaws.com/systemgravatars/avatar_6225.jpg").into(holder.ivBackground);
    }

    @Override
    public int getItemCount() {
        if(classes != null) {
            if(maxsize>classes.size()){
                maxsize = classes.size();
            }
        } else {
            maxsize = 0;
        }
        return maxsize;
    }

    public void setPagination(int pageNumber) {
        this.maxsize = pageNumber*4;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTeacher, tvDayLeft, tvDayClose, tvCategory, tvPrice, tvTitle, tvContent, tvCapacity;
        public ImageView ivAvatar, ivBackground;

        public CustomViewHolder(View view) {
            super(view);
            tvTeacher = view.findViewById(R.id.data_newsfeed_name);
            tvDayLeft = view.findViewById(R.id.data_newsfeed_daycreate);
            tvDayClose = view.findViewById(R.id.data_newsfeed_dayclose);
            Drawable img = mContext.getResources().getDrawable(R.drawable.ic_clock);
            img.setBounds( 0, 0, 30, 30 );
            tvDayClose.setCompoundDrawables( img, null, null, null );
            tvCategory = view.findViewById(R.id.data_newsfeed_category);
            tvPrice = view.findViewById(R.id.data_newsfeed_price);
            tvTitle = view.findViewById(R.id.data_newsfeed_title);
            tvContent = view.findViewById(R.id.data_newsfeed_content);
            tvCapacity = view.findViewById(R.id.data_newsfeed_capacity);
            Drawable img2 = mContext.getResources().getDrawable(R.drawable.ic_username);
            img2.setBounds( 0, 0, 30, 30 );
            tvCapacity.setCompoundDrawables( img2, null, null, null );
            ivAvatar = view.findViewById(R.id.data_newsfeed_avatar);
            ivBackground = view.findViewById(R.id.data_newsfeed_bg);
        }
    }
}

