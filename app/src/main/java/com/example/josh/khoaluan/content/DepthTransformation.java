package com.example.josh.khoaluan.content;

import android.support.v4.view.ViewPager;
import android.view.View;

public class DepthTransformation implements ViewPager.PageTransformer{
    @Override
    public void transformPage(View page, float position) {

        if (position < -1){    // [-Infinity,-1)
            // This page is way off-screen to the left.
            page.setAlpha(0f);

        }
        else if (position <= 0){    // [-1,0]
            page.setAlpha(1);
            page.setTranslationX(0);
            page.setScaleX(1);
            page.setScaleY(1);

        }
        else if (position <= 1){    // (0,1]
            page.setTranslationX(-position*page.getWidth());
            page.setAlpha(1-(Math.abs(position)/4));
            page.setScaleX(1-(Math.abs(position)/4));
            page.setScaleY(1-(Math.abs(position)/4));

        }
        else {    // (1,+Infinity]
            // This page is way off-screen to the right.
            page.setAlpha(0f);

        }


    }
}