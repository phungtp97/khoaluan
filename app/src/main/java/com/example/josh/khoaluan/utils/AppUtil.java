package com.example.josh.khoaluan.utils;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.example.josh.khoaluan.MVP.object.MyClass;
import com.example.josh.khoaluan.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by nhancao on 3/1/17.
 *
 * @author duonghd
 */

public class AppUtil {
    private static Context context;

    private static MyClass mClass;

    public static void init(Context context) {
        AppUtil.context = context;
    }

    /**
     * A method to find height of the status bar
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * Making notification bar transparent
     */
    public static void changeStatusBarColor(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.getWindow()
                    .getDecorView()
                    .setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    /**
     * Showing error validator message
     */
/*    public static void showValidationErrors(List<ValidationError> errors, Context context) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(context);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Log.d("validation message", message);
            }
        }
    }*/
    public static int dpToPx(Context context, float dp) {
        return (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics()));
    }

    public static int pxToDp(Context context, float px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static View getActivityRoot(Activity activity) {
        return ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
    }

    /**
     * hide keyboard when touch outside
     *
     * @param view = rootView
     */
    @SuppressLint("ClickableViewAccessibility")
    public static void setupUI(Context context,View view) {

        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                hideKeyBoard(context, v);
                return false;
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(context, innerView);
            }
        }
    }

    /**
     * clear focus when keyboard hide
     *
     * @param activity
     */
    public static void setupKeyboard(Activity activity) {
        KeyboardVisibility.setEventListener(activity, isOpen -> {
            if (!KeyboardVisibility.isKeyboardVisible(activity) && activity.getCurrentFocus() != null) {
                activity.getCurrentFocus().clearFocus();
            }
        });
    }

    public static void hideKeyBoard(Context context, View view) {
        if (context == null)
            throw new RuntimeException("Context is null, init(Context)");
        InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }

    public static AlertDialog showDialogOK(Context context, String message, DialogInterface.OnClickListener okListener,
                                           DialogInterface.OnClickListener cancelListener) {
        if (context == null) {
            return null;
        }
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", cancelListener)
                .show();
        return alertDialog;
    }

    public static int getHeightScreen() {
        if (context == null) throw new RuntimeException("Context is null, init(Context)");
        int height = 0;
        WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        if (Build.VERSION.SDK_INT > 12) {
            Point size = new Point();
            display.getSize(size);
            height = size.y;
        } else {
            height = display.getHeight(); // Deprecated
        }
        return height;
    }

    public static int getWidthScreen() {
        if (context == null) throw new RuntimeException("Context is null, init(Context)");
        int width = 0;
        WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        if (Build.VERSION.SDK_INT > 12) {
            Point size = new Point();
            display.getSize(size);
            width = size.x;
        } else {
            width = display.getWidth(); // Deprecated
        }
        return width;
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static void addFadingFeatureScrollView(Activity activity, NestedScrollView scrollView, ImageView imageView) {
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if(scrollView.getChildAt(0).getBottom()<= (scrollView.getHeight() + scrollView.getScrollY())){
                    imageView.setVisibility(View.INVISIBLE);
                }
                else{
                    imageView.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    public static void setViewClass(MyClass myClass){
        mClass = myClass;
    }
    public static MyClass getViewClass(){
        return mClass;
    }

    public static Date doDateSummation(String mmDDyyyy, int numberOfDays){
        SimpleDateFormat formatter = new SimpleDateFormat("mm/DD/yyyy");
        Date date = new Date();
        try {
            date = formatter.parse(mmDDyyyy);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, numberOfDays);
        date = c.getTime();
        return date;
    }

    public static float doDateSubstraction(String firstMMddYYYY, String secondMMddYYYY){
        SimpleDateFormat formatter = new SimpleDateFormat("mm/DD/yyyy");
        Date date1;
        Date date2;
        float result = 0;
        long diff = 0;
        try{
            date1 = formatter.parse(firstMMddYYYY);
            date2 = formatter.parse(secondMMddYYYY);
            diff = date2.getTime() - date1.getTime();
            result = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e){
            e.printStackTrace();
        }
        return result;
    }

    public static float getFromCurrentday(String MMddYYYY){
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-YYYY");
        Date date1 = new Date();
        Date date2 = new Date();
        float result = 0;
        long diff = 0;
        if(MMddYYYY!=null) {
            try {
                date1 = formatter.parse(formatter.format(date1));
                date2 = formatter.parse(MMddYYYY);
                Log.d("date", MMddYYYY +" " +date1 + " "+ date2.toString());
                diff = date2.getTime() - date1.getTime();
                result = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static String toMMddYYYY(String date){
        String result = "";
        if(date!=null){
        String day = date.substring(8, 10);
        String month = date.substring(5, 7);
        String year = date.substring(0,4);
        result =month+"-"+day+"-"+year;
        }
        return result;
    }

    public static void HeightChangedAnimation(Context context, View view, int duration, int heightInDp){
        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), dpToPx(context, heightInDp));
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = val;
                view.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(duration);
        anim.start();
    }

    public static void runSlideUpAnimation(Context context, View view, int duration){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        animation.setDuration(duration);
        view.startAnimation(animation);
        view.setVisibility(View.VISIBLE);
    }

    public static void runSlideDownAnimation(Context context, View view, int duration)
    {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        animation.setDuration(duration);
        view.startAnimation(animation);
        view.setVisibility(View.GONE);
    }

    public static void performDepthChangedOnPress(Context context, View view){
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float prevElevetion = v.getElevation();
                if (event.getAction()==MotionEvent.ACTION_DOWN) {
                    CountDownTimer countDownTimer =
                            new CountDownTimer((long) (prevElevetion*100), (long) (prevElevetion*10)) {

                                public void onTick(long millisUntilFinished) {
                                    v.setElevation(millisUntilFinished / prevElevetion*10);
                                }

                                public void onFinish() {
                                }

                            };
                    countDownTimer.start();
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    v.setElevation(prevElevetion);
                }
                    return false;
            }
        });
    }

}

