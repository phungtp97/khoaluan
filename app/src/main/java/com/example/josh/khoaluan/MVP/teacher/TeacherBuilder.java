package com.example.josh.khoaluan.MVP.teacher;


import android.app.ProgressDialog;
import android.content.Context;

public class TeacherBuilder {
    Teacher mTeacher;
    Context mContext;
    String savedPassword;
    String savedUsername;
    ProgressDialog pDialog;
    private String teacherID;
    private String teacherEmail;
    private String teacherPhone;
    private String teacherName;
    private String teacherBirth;
    private Boolean teacherGender;
    private Double teacherPrice;
    private Integer personalRate;
    private String teacherDetail;
    private Boolean teacherActive;
    private Boolean teacherVerify;
    private String teacherPic;
    private String teacherPassword;
    Boolean result;
    String message;

    public TeacherBuilder(Teacher teacher, Context context) {
        this.mTeacher = teacher;
        this.mContext = context;
        mTeacher = new Teacher();
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public void setTeacherPhone(String teacherPhone) {
        this.teacherPhone = teacherPhone;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public void setTeacherBirth(String teacherBirth) {
        this.teacherBirth = teacherBirth;
    }

    public void setTeacherGender(Boolean teacherGender) {
        this.teacherGender = teacherGender;
    }

    public void setTeacherPrice(Double teacherPrice) {
        this.teacherPrice = teacherPrice;
    }

    public void setPersonalRate(Integer personalRate) {
        this.personalRate = personalRate;
    }

    public void setTeacherDetail(String teacherDetail) {
        this.teacherDetail = teacherDetail;
    }

    public void setTeacherActive(Boolean teacherActive) {
        this.teacherActive = teacherActive;
    }

    public void setTeacherVerify(Boolean teacherVerify) {
        this.teacherVerify = teacherVerify;
    }

    public void setTeacherPic(String teacherPic) {
        this.teacherPic = teacherPic;
    }

    public void setTeacherPassword(String teacherPassword) {
        this.teacherPassword = teacherPassword;
    }
}
