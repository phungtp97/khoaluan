package com.example.josh.khoaluan;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.object.BaseDialog;
import com.example.josh.khoaluan.MVP.object.BaseDialogListener;
import com.example.josh.khoaluan.MVP.teacher.Teacher;
import com.example.josh.khoaluan.base.BaseActivity;
import com.example.josh.khoaluan.content.ContentActivity_;
import com.example.josh.khoaluan.login.LoginLearnerPresenter;
import com.example.josh.khoaluan.login.LoginTeacherPresenter;
import com.example.josh.khoaluan.login.LoginView;
import com.example.josh.khoaluan.login.signup.SignUpActivity_;
import com.example.josh.khoaluan.passwordconfig.OtpCodeActivity_;
import com.example.josh.khoaluan.accountservices.UserSingleton;
import com.example.josh.khoaluan.utils.AppUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements LoginView, BaseDialogListener {

    @ViewById(R.id.signin_et_username)
    EditText etUsername;

    @ViewById(R.id.signin_et_password)
    EditText etPassword;

    @ViewById(R.id.signin_btn_login)
    TextView btnLogIn;

    @ViewById(R.id.sign_in_isteacher)
    Switch swIsTeacher;

    @ViewById(R.id.signin_tv_go_to_sign_up)
    TextView btnGoToSignUp;

    @ViewById(R.id.signin_btn_forgot)
    TextView btnForgot;

    BaseDialog dialog;
    @AfterViews
    public void doAfterViews(){
        AppUtil.setupUI(this, AppUtil.getActivityRoot(this));
    }

    @Click(R.id.signin_btn_login)
    public void clickLogin(){
        if(swIsTeacher.isChecked()){
            LoginTeacherPresenter presenter = new LoginTeacherPresenter(this, this);
            presenter.loadData(etUsername.getText().toString(), etPassword.getText().toString());
        }
        else{
            LoginLearnerPresenter presenter = new LoginLearnerPresenter(this, this);
            presenter.loadData(etUsername.getText().toString(), etPassword.getText().toString());
        }
    }

    @Click(R.id.signin_tv_go_to_sign_up)
    public void goSignup(){
        SignUpActivity_.intent(this).start();
    }

    @Click(R.id.signin_btn_forgot)
    public void goForgotPassword() {
         dialog = new BaseDialog(this)
                .setHintContent("Enter your username")
                .setLabel("Forgot your password ?")
                .setClickListener(this);
         dialog.show();
    }
    //Show learner presenter result
    @Override
    public void onDisplayLearner(Learner learner) {
        if(learner!=null){
            UserSingleton.setLearnerUser(learner);
            if(UserSingleton.getUserLearner()!=null){
                Toast.makeText(this, "Load Learner Success", Toast.LENGTH_LONG).show();
                ContentActivity_.intent(this).start();
            }
            else{
                Toast.makeText(this, "Load Learner Failed", Toast.LENGTH_LONG).show();
            }
        }
    }

    //Show teacher presenter result
    @Override
    public void onDisplayTeacher(Teacher teacher) {
        if(teacher!=null){
            UserSingleton.setTeacherUser(teacher);
            if(UserSingleton.getUserTeacher()!=null){
                Toast.makeText(this, "Load Teacher Success", Toast.LENGTH_LONG).show();
                ContentActivity_.intent(this).start();
            }
            else {
                Toast.makeText(this, "Load Teacher Success", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onDisplayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void doOnClickButtonLeft(String content) {
        OtpCodeActivity_.intent(this).start();
    }

    @Override
    public void doOnClickButtonRight() {

    }
}