package com.example.josh.khoaluan.MVP.view;

import android.support.annotation.Nullable;

import com.example.josh.khoaluan.MVP.object.MyClass;

import java.util.List;

public interface ViewMVP {
    void onDisplay(@Nullable List<MyClass> classList);
}
