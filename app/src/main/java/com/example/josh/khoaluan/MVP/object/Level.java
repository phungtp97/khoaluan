package com.example.josh.khoaluan.MVP.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Level {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("levelDebscibe")
    @Expose
    private String levelDebscibe;

    public String getLevelDebscibe() {
        return levelDebscibe;
    }

    @Override
    public String toString() {
        return levelDebscibe;
    }
}