package com.example.josh.khoaluan.login;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.josh.khoaluan.MVP.learner.Learner;
import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.accountservices.UserSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginLearnerRequest {
    LoginLearnerListener loginLearnerListener;
    Context context;
    ProgressDialog pDialog;

    public LoginLearnerRequest(LoginLearnerListener loginLearnerListener, Context context) {
        this.loginLearnerListener = loginLearnerListener;
        this.context = context;
    }
    public void logIn(String id, final String password){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Data.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        Apiservice api = RetrofitClient.getApiService();

        Call<Learner> call = api.getLearnerById(id);

        call.enqueue(new Callback<Learner>() {
            @Override
            public void onResponse(Call<Learner> call, Response<Learner> response) {
                pDialog.dismiss();
                if(response.body()!=null) {
                    if (response.body().getLearnerPassword().replaceAll(" ","").equals(password)) {
                        pDialog.dismiss();
                            loginLearnerListener.onLoadLearnerSucceed(response.body());
                            UserSingleton.USER_TYPE = 1;
                    }
                    else{
                        pDialog.dismiss();
                        loginLearnerListener.onLoadLearnerFailed("Wrong password");
                    }
                }
                else{
                    pDialog.dismiss();
                    loginLearnerListener.onLoadLearnerFailed("Wrong username");
                }
            }

            @Override
            public void onFailure(Call<Learner> call, Throwable t) {
                loginLearnerListener.onLoadLearnerFailed(t.getMessage());
            }
        });
    }

}
