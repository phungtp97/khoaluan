package com.example.josh.khoaluan.classmain;

import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.object.BaseDialog;
import com.example.josh.khoaluan.MVP.object.BaseDialogListener;
import com.example.josh.khoaluan.MVP.object.CategoryApi;
import com.example.josh.khoaluan.MVP.object.Level;
import com.example.josh.khoaluan.MVP.object.MyClass;
import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.accountservices.UserSingleton;
import com.example.josh.khoaluan.content.ContentActivity_;
import com.example.josh.khoaluan.utils.AppUtil;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.shawnlin.numberpicker.NumberPicker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@EActivity(R.layout.activity_create_class)
public class CreateClassActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, CreateClassView, BaseDialogListener {

    BaseDialog dialogEditText;
    @ViewById(R.id.createclass_tv_edittitle)
    TextView tvTitle;
    @ViewById(R.id.createclass_tv_editdetail)
    TextView tvDetail;
    @ViewById(R.id.createclass_category_spinner)
    Spinner spnCategory;
    @ViewById(R.id.createclass_level_spinner)
    Spinner spnLevel;

    @NotEmpty
    @ViewById(R.id.createclass_tv_day)
    EditText tvDay;

    @ViewById(R.id.createclass_edit_price_capacity)
    LinearLayout llEditPriceCapacity;

    @ViewById(R.id.np_price)
    EditText npPrice;

    @ViewById(R.id.np_min_num)
    NumberPicker npMin;

    @ViewById(R.id.np_max_num)
    NumberPicker npMax;

    @ViewById(R.id.createclass_panel_tv_price)
    TextView tvPricePanel;

    @ViewById(R.id.createclass_panel_tv_capacity)
    TextView tvCapacityPanel;

    @NotEmpty
    @ViewById(R.id.createclass_tv_capacity)
    EditText tvCapacity;

    @NotEmpty
    @ViewById(R.id.createclass_tv_price)
    EditText tvPrice;

    @ViewById(R.id.createclass_message_ll)
    LinearLayout llMessagePopup;

    @ViewById(R.id.createclass_go_browser)
    TextView btnGoBrowser;

    @ViewById(R.id.createclass_go_classdetail)
    TextView btnGoViewClass;


    String[] categoryStringList;

    String[] levelStringList;

    @ViewById(R.id.createclass_np_availableday)
    NumberPicker npAvailableDay;

    @ViewById(R.id.createclass_tv_availableday)
    TextView tvAvaialableDay;

    int minNum;

    int maxNum;

    int availableDay;

    String price = "0";

    private MyClass myClass;

    @AfterViews
    public void init() {
        myClass = new MyClass();


        dialogEditText = new BaseDialog(this);
        dialogEditText.setClickListener(this);
        npPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tvPricePanel.setText("Price: $" + s);
                tvPrice.setText(s);
                price = s.toString();
            }
        });

        npMin.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                minNum = newVal;
                tvCapacityPanel.setText("Class capacity: from " + minNum + " to " + maxNum);
                tvCapacity.setText(minNum + "/" + maxNum);
                myClass.setMinimumCapacity(minNum);
                myClass.setMaximumCapacity(maxNum);
            }
        });
        npMax.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                maxNum = newVal;
                tvCapacityPanel.setText("Class capacity: from " + minNum + " to " + maxNum);
                tvCapacity.setText(minNum + "/" + maxNum);
                myClass.setMinimumCapacity(minNum);
                myClass.setMaximumCapacity(maxNum);
            }
        });

        npAvailableDay.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                availableDay = newVal;
                if (availableDay <= 1) {
                    tvAvaialableDay.setText(availableDay + " day");
                } else {
                    tvAvaialableDay.setText(availableDay + " days");
                }
            }
        });

        Apiservice api = RetrofitClient.getApiService();
        Call<List<CategoryApi>> listCategories = api.getAllCatgories();
        listCategories.enqueue(new Callback<List<CategoryApi>>() {
            @Override
            public void onResponse(Call<List<CategoryApi>> call, Response<List<CategoryApi>> response) {
                if (response.body() != null) {
                    spnCategory.setAdapter(null);
                    Toast.makeText(CreateClassActivity.this, "Get category list succeed", Toast.LENGTH_LONG).show();
                    categoryStringList = new String[response.body().size()];
                    for (int i = 0; i < response.body().size(); i++) {
                        categoryStringList[i] = (response.body().get(i).getCategory1());
                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(CreateClassActivity.this, R.layout.spinner_item, categoryStringList);
                    spnCategory.setAdapter(spinnerArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<CategoryApi>> call, Throwable t) {
                Toast.makeText(CreateClassActivity.this, "Get level list failed", Toast.LENGTH_LONG).show();
            }
        });

        Call<List<Level>> listLevels = api.getAllLevels();
        listLevels.enqueue(new Callback<List<Level>>() {
            @Override
            public void onResponse(Call<List<Level>> call, Response<List<Level>> response) {
                if (response.body() != null) {
                    spnCategory.setAdapter(null);
                    Toast.makeText(CreateClassActivity.this, "Get category list succeed", Toast.LENGTH_LONG).show();
                    levelStringList = new String[response.body().size()];
                    for (int i = 0; i < response.body().size(); i++) {
                        levelStringList[i] = (response.body().get(i).getLevelDebscibe());
                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(CreateClassActivity.this, R.layout.spinner_item, levelStringList);
                    spnLevel.setAdapter(spinnerArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Level>> call, Throwable t) {
                Toast.makeText(CreateClassActivity.this, "Get level list failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Click(R.id.create_class_datepicker)
    public void initDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                CreateClassActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Click(R.id.createclass_btn_edittitle)
    public void editTitle() {
        dialogEditText
                .setContent("", true)
                .setLabel("Set title")
                .setTag(1);
        dialogEditText.show();
    }

    @Click(R.id.createclass_btn_editdetail)
    public void editDetail() {
        dialogEditText
                .setContent("", true)
                .setLabel("Set details")
                .setTag(2);
        dialogEditText.show();
    }

    @Click(R.id.createclass_btn_editcategory)
    public void editCategory() {
        spnCategory.performClick();
    }

    @Click(R.id.createclass_btn_editlevel)
    public void editLevel() {
        spnLevel.performClick();
    }

    @Click(R.id.createclass_btn_editnumber)
    public void editNumber() {
        llEditPriceCapacity.setVisibility(View.VISIBLE);
    }

    @Click(R.id.createclass_btn_editprice)
    public void editPrice() {
        llEditPriceCapacity.setVisibility(View.VISIBLE);
    }

    @Click(R.id.createclass_opacitypart)
    public void closeEditPriceCapacity() {
        llEditPriceCapacity.setVisibility(View.GONE);
    }

    @Click(R.id.create_class_pricecapacity_ok)
    public void okPriceCapacity() {
        llEditPriceCapacity.setVisibility(View.GONE);
    }

    @Click(R.id.create_new_class)
    public void createNewClass() {
        myClass.setCategory(spnCategory.getSelectedItem().toString());
        myClass.setLevelDebscibe(spnLevel.getSelectedItem().toString());
        myClass.setClassTitle(tvTitle.getText().toString());
        myClass.setClassDetail(tvDetail.getText().toString());
        myClass.setClassPrice(Float.parseFloat(price));
        myClass.setClassID(UUID.randomUUID().toString());
        myClass.setTeacherID(UserSingleton.getUserTeacher().getTeacherID());
        CreateClassPresenter presenter = new CreateClassPresenter(this, this);
        presenter.loadData(myClass);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        tvDay.setText("from " + monthOfYear + "/" + dayOfMonth + "/" + year + " to " + monthOfYearEnd + "/" + dayOfMonthEnd + "/" + yearEnd);
        myClass.setStartDay(monthOfYear + "/" + dayOfMonth + "/" + year);
        myClass.setEndDay(monthOfYearEnd + "/" + dayOfMonthEnd + "/" + yearEnd);
    }

    @Override
    public void onDisplayClass(MyClass myClass) {
        AppUtil.runSlideUpAnimation(this, llMessagePopup, 1000);
    }

    @Override
    public void onDisplayError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Click(R.id.createclass_go_browser)
    public void goContent(){
        ContentActivity_.intent(this).start();
    }

    @Override
    public void doOnClickButtonLeft(String content) {
        if (dialogEditText.getTag() == 1) {
            tvTitle.setText(content);
        } else {
            tvDetail.setText(content);
        }
        dialogEditText.dismiss();
    }

    @Override
    public void doOnClickButtonRight() {
        dialogEditText.dismiss();
    }
}
