package com.example.josh.khoaluan.content;


import com.example.josh.khoaluan.MVP.object.MyClass;

import java.util.List;

public interface AllClassListener {
    void onLoadAllClassSucceed(List<MyClass> classList);
    void onLoadAllClassFailed(String message);
}
