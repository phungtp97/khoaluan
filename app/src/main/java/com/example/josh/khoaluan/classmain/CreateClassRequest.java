package com.example.josh.khoaluan.classmain;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.josh.khoaluan.MVP.object.RetrofitClient;
import com.example.josh.khoaluan.MVP.object.Apiservice;
import com.example.josh.khoaluan.MVP.object.MyClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateClassRequest {
    CreateClassListener classListener;
    Context context;
    ProgressDialog pDialog;

    public CreateClassRequest(CreateClassListener classListener, Context context) {
        this.classListener = classListener;
        this.context = context;
    }
    public void loadData(MyClass myClass){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Data.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        Apiservice api = RetrofitClient.getApiService();
        Call<MyClass> call = api.postClass( myClass);
        call.enqueue(new Callback<MyClass>() {
            @Override
            public void onResponse(Call<MyClass> call, Response<MyClass> response) {
                if(response.body()!=null) {
                    pDialog.dismiss();
                    classListener.onCreateClassSucceed(response.body());
                }
                else
                {
                    pDialog.dismiss();
                    classListener.onCreateClassFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<MyClass> call, Throwable t) {
                pDialog.dismiss();
                classListener.onCreateClassFailed(t.getMessage());
            }
        });
    }
}
