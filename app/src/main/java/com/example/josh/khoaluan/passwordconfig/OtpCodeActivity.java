package com.example.josh.khoaluan.passwordconfig;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josh.khoaluan.EhancedEditText.EhancedEditTextBuilder;
import com.example.josh.khoaluan.EhancedEditText.EhancedEditTextLayout;
import com.example.josh.khoaluan.EhancedEditText.onCheckLimitedInputListener;
import com.example.josh.khoaluan.MVP.object.BaseDialog;
import com.example.josh.khoaluan.R;
import com.example.josh.khoaluan.base.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_otp_code)
public class OtpCodeActivity extends BaseActivity{
    @ViewById(R.id.otpcode_fl_enterpassword)
    FrameLayout flEnterPasscode;

    @ViewById(R.id.otpcode_btn_submit)
    TextView btnSubmitCode;

    @ViewById(R.id.otpcode_fl_blurry)
    FrameLayout flBurry;

    @ViewById(R.id.otpcode_form_ll)
    LinearLayout llForm;

    @ViewById(R.id.otpcode_textdummy)
    EditText etDummy;
    List<EditText> editTexts = new ArrayList<>();
    @AfterViews
    public void init(){
        for(int i = 0; i< 6; i++){
            EditText editText = new EditText(this);
            editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f));
            editText.setGravity(Gravity.CENTER);
            editText.setTextColor(Color.BLACK);
            editTexts.add(editText);
            llForm.addView(editText,i);
        }
        flBurry.setAlpha(0.5f);

        getDummy();
    }

    int prevLength;
    int length;

    public void getDummy(){
        etDummy.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                prevLength = count;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count>0) {
                    editTexts.get(count - 1).setText(etDummy.getText().toString().substring(count - 1));
                }
                length = count;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(prevLength > length){
                    editTexts.get(length).setText("");
                }
            }
        });
    }

    @Click(R.id.otpcode_btn_submit)
    public void onClickSubmit(){
        flBurry.setAlpha(1.0f);
        flEnterPasscode.setVisibility(View.GONE);
    }

}
