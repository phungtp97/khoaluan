package com.example.josh.khoaluan.MVP.object;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmallInfoClass {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("teacherID")
    @Expose
    private String teacherID;
    @SerializedName("teachername")
    @Expose
    private String teachername;
    @SerializedName("teacherPic")
    @Expose
    private Uri teacherPic;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getTeachername() {
        return teachername;
    }

    public void setTeachername(String teachername) {
        this.teachername = teachername;
    }

    public Uri getTeacherPic() {
        return teacherPic;
    }

    public void setTeacherPic(Uri teacherPic) {
        this.teacherPic = teacherPic;
    }

}
