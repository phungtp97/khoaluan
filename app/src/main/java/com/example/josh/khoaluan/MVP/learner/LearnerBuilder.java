package com.example.josh.khoaluan.MVP.learner;

import android.app.ProgressDialog;
import android.content.Context;

public class LearnerBuilder {
    Learner mLearner;
    String userID;
    String email;
    String phone;
    String name;
    String birth;
    Float balance;
    Boolean verify;
    Boolean active;
    String password;
    String savedPassword;
    String savedUsername;
    String savedEmail;
    String savedPhone;
    boolean result;
    String message;
    Context mContext;
    ProgressDialog pDialog;
    public LearnerBuilder(Learner learner, Context context) {
        this.mLearner = learner;
        this.mContext = context;
        mLearner = new Learner();
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public LearnerBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public LearnerBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public LearnerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public LearnerBuilder setBirth(String birth) {
        this.birth = birth;
        return this;
    }

    public LearnerBuilder setBalance(Float balance) {
        this.balance = balance;
        return this;
    }

    public LearnerBuilder setVerify(Boolean verify) {
        this.verify = verify;
        return this;
    }

    public LearnerBuilder setActive(Boolean active) {
        this.active = active;
        return this;
    }

    public LearnerBuilder setPassword(String password) {
        this.password = password;
        return this;
    }
    private Learner build(){
        mLearner.setLearnerID(userID);
        mLearner.setLearnerEmail(email);
        mLearner.setLearnerPhone(phone);
        mLearner.setPersonalBalance(balance);
        mLearner.setLearnerVerify(verify);
        mLearner.setLearnerActive(active);
        mLearner.setLearnerPassword(password);
        return mLearner;
    }


    protected void setmLearner(Learner learner){
        mLearner = learner;
    }

    protected Learner getmLearner(){
        return mLearner;
    }

    protected Learner getUserByID(){
        return mLearner;
    }

/*
    public void signUp(String userID, String email, String phone, String password){
        this.savedUsername = userID;
        this.savedPassword = password;
        this.savedEmail = email;
        this.savedPhone = phone;
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Loading Data.. Please wait...\nUsername: "+savedUsername
                +"\nUserEmail: " + savedEmail
                +"\nsavedPhone: " + savedPhone
                +"\nsavedPassword" + savedPassword);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        Apiservice api = RetrofitClient.getApiService();
        Call<Learner> call = api.saveLearnerPut(savedUsername, savedUsername, savedEmail, savedPhone, savedPassword);
        call.enqueue(new Callback<Learner>() {
            @Override
            public void onResponse(Call<Learner> call, Response<Learner> response) {
                if(response.isSuccessful()) {
                    pDialog.dismiss();
                    Toast.makeText(mContext, "Sign up success", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Learner> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(mContext, "Sign up failed \n"+t.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });

    }
*/
}
